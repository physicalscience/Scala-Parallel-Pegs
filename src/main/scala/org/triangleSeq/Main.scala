package org.triangleSeq

import javax.swing.JFrame

/**
  * Main Object to start the program
  */
object Main extends App{
  val frame = new ParFrame
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
  frame.pack()
  frame.setVisible(true)
}
