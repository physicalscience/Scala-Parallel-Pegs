package org.triangleSeq

import java.awt.Dimension
import java.awt.event.{ActionEvent, ActionListener}
import java.util.concurrent.{ForkJoinPool, ForkJoinTask}
import javax.swing._

import net.miginfocom.swing.MigLayout

import scala.collection.parallel.mutable.ParArray

/**
  * Panel that actually plays out the game
  */
class PlayPanel(presenter: MainPresenter, frame: MainWindow, parLevel: Int) extends JPanel {
  this.setLayout(new MigLayout())
  val pegSelector = new JComboBox[Int]()
  val emptySelector = new JComboBox[Int]()
  val moveExecute = new JButton("Make Move")
  val helpButton = new JButton("Help Me!")
  val pool = new ForkJoinPool(parLevel)
  val myself = this
  this.add(new JLabel("Peg Selector"), "gapleft 50, gapright 50")
  this.add(new JLabel("Spot to Jump"), "wrap")
  this.add(pegSelector, "gapleft 50, gapright 50")
  this.add(emptySelector, "wrap")
  this.add(moveExecute, "gapleft 80, wrap")
  this.add(helpButton, "gapleft 80")
  pegSelector.setPreferredSize(new Dimension(15, 10))
  emptySelector.setPreferredSize(new Dimension(15, 10))
  this.setPegs(presenter)

  moveExecute.addActionListener(new ActionListener {
    override def actionPerformed(e: ActionEvent): Unit = {
      presenter.pegs(pegSelector.getSelectedItem.asInstanceOf[Int]).killMap.get(emptySelector.getSelectedItem.asInstanceOf[Int]) match {
        case Some(o) =>
          presenter.pegs.filter(p => p.slot.equals(o)).head.alive = false
          presenter.pegs(emptySelector.getSelectedItem.asInstanceOf[Int]).alive = true
          presenter.pegs(pegSelector.getSelectedItem.asInstanceOf[Int]).alive = false
        case None =>
          JOptionPane.showMessageDialog(frame, "That is not a legal Move!")

      }
    }
  })

  helpButton.addActionListener(new ActionListener {
    override def actionPerformed(e: ActionEvent): Unit = {
      val playMap = new scala.collection.concurrent.TrieMap[String, Boolean]()
      val newPres = new MainPresenter
      for(i <- newPres.pegs.indices) {
        newPres.pegs(i).alive = presenter.pegs(i).alive
      }
      val t = new RunGame(newPres, parLevel, playMap)
      val win = t.startRun(parLevel)
      JOptionPane.showMessageDialog(myself, "There are " + win + " possible paths to victory from this state!")
    }
  })

  /**
    * Sets the pegs on the screen to represent the current state of the game
    * @param presenter Takes in a MainPresenter
    */
  def setPegs(presenter: MainPresenter) = {
    val dead: Array[Peg] = presenter.collectDead
    val pegsToJump = presenter.getPegs.par.filter(p => {
      var decider: Boolean = false
      dead.foreach(d => {
        if(p.killSpots.contains(d.slot) && p.alive) decider = true
      })
      decider
    })
    fillPeg(dead.map(f => f.slot))
    jumpPeg(pegsToJump.map(f => f.slot))
    presenter.setColors
    frame.gamePanel.repaint()
    this.repaint()
  }

  /**
    * Fills the comboBox of empty peg slots to pick
    * @param dead takes in an array of dead peg slots
    */
  def fillPeg(dead: Array[Int]): Unit = {
    emptySelector.removeAllItems()
    dead.foreach(f => emptySelector.addItem(f))
    emptySelector.repaint()
  }

  /**
    * Fills the comboBox of pegs that you can use to jump
    * @param pegsToJump takes in an array of pegs that are able to jump
    */
  def jumpPeg(pegsToJump: ParArray[Int]): Unit = {
    pegSelector.removeAllItems()
    pegsToJump.foreach(f => pegSelector.addItem(f))
    pegSelector.repaint()
  }
}
