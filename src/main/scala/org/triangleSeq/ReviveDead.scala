package org.triangleSeq

import java.util.concurrent.Callable

/**
  * Small task that revives the dead peg that is now filled after a jump
  */
class ReviveDead(presenter: MainPresenter, playPanel: PlayPanel) extends Callable[Unit]{
  override def call(): Unit = {
    presenter.pegs(playPanel.emptySelector.getSelectedItem.asInstanceOf[Int]).alive = true
  }
}
