package org.triangleSeq

import java.awt.{Color, Graphics, Graphics2D}
import javax.swing.JPanel

/**
  * Panel that paints the board onto the Frame
  */
class GamePanel(presenter: MainPresenter) extends JPanel{

  override def paintComponent(g: Graphics): Unit = {
    super.paintComponent(g)
    val g2 = g.asInstanceOf[Graphics2D]
    for (i <- 0 until 15) presenter.paint(g2, presenter.pegs(i).point._1, presenter.pegs(i).point._2, presenter.pegs(i).color)
  }
}
