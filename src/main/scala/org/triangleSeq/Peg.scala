package org.triangleSeq

import java.awt.Color

import scala.collection.immutable.HashMap

/**
  * Object that represents one actual peg on the board and all the necessary information needed
  */
class Peg(a: Boolean, kS: Array[Int], p: (Int, Int), s: Int, hm: HashMap[Int, Int], stringNum: String) {
  var color: Color = _
  var alive: Boolean = a
  val killSpots: Array[Int] = kS
  val point: (Int, Int) = p
  val slot: Int = s
  val killMap: HashMap[Int, Int] = hm
  val slotToString = stringNum
}
