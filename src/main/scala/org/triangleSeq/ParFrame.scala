package org.triangleSeq

import java.awt.Dimension
import java.awt.event.{ActionEvent, ActionListener}
import javax.swing.event.{ChangeEvent, ChangeListener}
import javax.swing._

import net.miginfocom.swing.MigLayout

/**
  * Small Little frame that pops up and allows you to set the depth of the recursion
  */
class ParFrame extends JFrame {
  var parLevel: Int = 3
  val parButton = new JButton("Play!")
  val myself = this
  var depth = new JTextField()
  depth.setPreferredSize(new Dimension(25, 10))
  this.setLayout(new MigLayout())
  this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
  this.setLocationRelativeTo(null)
  this.add(new JLabel("Select Level of parallelism"), "gapleft 50, wrap")
  this.add(depth, "gapleft 50, wrap")
  this.add(parButton, "gapleft 50")

  parButton.addActionListener(new ActionListener {
    override def actionPerformed(e: ActionEvent): Unit = {
      parLevel = Integer.parseInt(depth.getText)
      val frame = new MainWindow(new MainPresenter, parLevel)
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
      frame.setLocationRelativeTo(myself)
      frame.pack()
      frame.setVisible(true)
      myself.setVisible(false)
    }
  })
}
