package org.triangleSeq

import java.awt.{Color, Graphics2D}

import scala.collection.immutable.HashMap
import scala.util.Random

/**
  * The presenter holds all the information about the pegs on the board
  */
class MainPresenter {
  val random = new Random()
  var pegs = new Array[Peg](15)
  pegs(0) = new Peg(false, Array(5, 3), (87, 0), 0, HashMap((5, 2), (3, 1)), "zero")
  pegs(1) = new Peg(true, Array(6, 8), (60, 35), 1, HashMap((6, 3), (8, 4)), "one")
  pegs(2) = new Peg(true, Array(9, 7), (112, 35), 2, HashMap((9, 5), (7, 4)), "two")
  pegs(3) = new Peg(true, Array(0, 10, 12, 5), (46, 73), 3, HashMap((0, 1), (10, 6), (12, 7), (5, 4)), "three")
  pegs(4) = new Peg(true, Array(11, 13), (87, 73), 4, HashMap((11, 7), (13, 8)), "four")
  pegs(5) = new Peg(true, Array(0, 14, 3, 12), (128, 73), 5, HashMap((0, 2), (14, 9), (3, 4), (12, 8)), "five")
  pegs(6) = new Peg(true, Array(1, 8), (23, 111), 6, HashMap((1, 3), (8, 7)), "six")
  pegs(7) = new Peg(true, Array(2, 9), (64, 111), 7, HashMap((2, 4), (9, 8)), "seven")
  pegs(8) = new Peg(true, Array(1, 6), (105, 111), 8, HashMap((1, 4), (6, 7)), "eight")
  pegs(9) = new Peg(true, Array(2, 7), (146, 111), 9, HashMap((2, 5), (7, 8)), "nine")
  pegs(10) = new Peg(true, Array(3, 12), (0, 149), 10, HashMap((3, 6), (12, 11)), "ten")
  pegs(11) = new Peg(true, Array(4, 13), (41, 149), 11, HashMap((4, 7), (13, 12)), "eleven")
  pegs(12) = new Peg(true, Array(5, 14, 3, 10), (82, 149), 12, HashMap((5, 8), (14, 13), (3, 7), (10, 11)), "twelve")
  pegs(13) = new Peg(true, Array(4, 11), (123, 149), 13, HashMap((4, 8), (11, 12)), "thirteen")
  pegs(14) = new Peg(true, Array(5, 12), (164, 149), 14, HashMap((5, 9), (12, 13)), "fourteen")
  this.setColors

  /**
    * changes the colors of the pegs based on whether they are alive or dead
    */
 def setColors = {
   pegs.foreach(p => {
     if(p.alive) p.color = Color.CYAN
     else p.color = Color.BLACK
   })
 }

  /**
    * Sets the pegs
    * @param p takes in an Array of Pegs
    */
  def setPegs(p: Array[Peg]) = {pegs = p}

  /**
    * collects all the alive pegs on the board
    * @return returns an Array of alive pegs
    */
  def collectAlive: Array[Peg] = pegs.filter(p => p.alive)

  /**
    * Collects all the dead pegs on the board
    * @return returns an Array of pegs
    */
  def collectDead: Array[Peg] = pegs.filter(p => !p.alive)

  /**
    * collects all the slots of the board
    * @return returns an Array of slot numbers
    */
  def collectSlots: Array[Int] = collectAlive.map(p => p.slot)

  /**
    * Gets the collection of pegs
    * @return returns an Array of pegs
    */
  def getPegs: Array[Peg] = pegs

  /**
    * paints the screen
    * @param g2 takes in a 2DGraphics object
    * @param x takes in an x coordinate
    * @param y takes in a y coordinate
    * @param c takes in a Color object
    */
  def paint(g2: Graphics2D, x: Int, y: Int, c: Color) = {
    g2.setColor(c)
    g2.fillOval(x, y, 35, 35)
  }
}
