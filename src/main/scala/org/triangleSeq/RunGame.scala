package org.triangleSeq

import java.util
import java.util.concurrent.{Callable, ForkJoinTask}

/**
  * Actual class that runs through and attempts to find all possible win conditions
  */
class RunGame(pres: MainPresenter, l: Int, playMap: scala.collection.concurrent.TrieMap[String, Boolean]) extends Callable[Int] {
  /**
    * The initial start call
    * @return returns the number of wins that it finds
    */
  override def call(): Int = {
    startRun(l)
  }

  /**
    * recursively goes through and tries to find every possible win
    * @param limit takes in the depth limit
    * @return returns the number of win states that it found
    */
  def startRun(limit: Int): Int = {
    val dead = pres.collectDead
    val pegsToJump = pres.getPegs.filter(p => {
      var decider: Boolean = false
      dead.foreach(d => {
        if(p.killSpots.contains(d.slot) && p.alive && pres.pegs(p.killMap.get(d.slot).get).alive) decider = true
      })
      decider
    })
    if(pegsToJump.length.equals(0) || limit.equals(0)) {
      if(limit.equals(0)) 0
      else if(dead.length.equals(14) && pres.collectAlive.length.equals(1)) 1
      else 0
    }
    else {
        val nl = limit - 1
        val stack = new util.LinkedList[ForkJoinTask[Int]]()
        dead.foreach(d => {
          pegsToJump.foreach(p => {
            p.killMap.get(d.slot) match {
              case Some(i) =>
                val newPres = new MainPresenter
                val pegs = newPres.pegs
                val oldPegs = pres.pegs
                var state: String = ""
                for(i <- 0 until pegs.length) {
                  pegs(i).alive = oldPegs(i).alive
                }
                pegs(d.slot).alive = true
                pegs(p.slot).alive = false
                pegs(i).alive = false
                for(i <- pegs.indices) {
                  if(pegs(i).alive) state = state + pegs(i).slotToString
                }
                playMap.get(state) match {
                  case Some(p) => //do nothing
                  case None =>
                    playMap.put(state, true)
                    val t = ForkJoinTask.adapt(new RunGame(newPres, nl, playMap))
                    t.fork()
                    stack.push(t)
                }
              case None => //nothing, should be impossible to get here
            }
          })
        })
        var count = 0
        while(!stack.isEmpty) count += stack.pop().join()
        count
      }
  }
}
