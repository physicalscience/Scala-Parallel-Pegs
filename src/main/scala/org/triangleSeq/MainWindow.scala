package org.triangleSeq

import java.awt.Dimension
import javax.swing.{JFrame, JLabel, JPanel}

import net.miginfocom.swing.MigLayout

/**
  * The Main window for the entire program
  */
class MainWindow(presenter: MainPresenter, parLevel: Int) extends JFrame {
  this.setLayout(new MigLayout())
  this.setPreferredSize(new Dimension(400, 500))
  var gamePanel = new GamePanel(presenter)
  var playPanel = new PlayPanel(presenter, this, parLevel)
  gamePanel.setPreferredSize(new Dimension(200, 200))
  this.add(new JLabel("PEG SOLITAIRE"), "gapleft 145, wrap")
  this.add(gamePanel, "gapleft 80, gaptop 50, wrap")
  this.add(playPanel, "gapleft 40, gaptop 15")
}
